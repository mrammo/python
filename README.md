Hangman Game rules:

Computer chooses randomly one word, which you have to guess.

You have to write a letter to entry box and click button "Guess a letter". If that letter is in the word then you will see this letter in your word. If that letter is not in the word then computer adds a body part to the gallows (head, body, left arm, right arm, left leg, right leg).

You will continue guessing letters until you either solve the word or all six body parts are on the gallows.

You can entry only one letter at the time. 
You win when you have solved the word. 
Bottom of the game you see the letters which you have already guessed and you can't guess them again.


How to run:

python projekt_graafiline.py


How to run unit test:

python -m unittest projekt_graafiline.py
