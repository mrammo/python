import time

print("Welcome to the Hangman Game!")
print("")
time.sleep(1)

word = "computer"
word = word.upper()

valesti_pakutud_tahti = 0
pakutud_tahed = []

letters = list(word)

arvatav_sona = ["-"]*len(letters)

arvatav_sona[0] = letters[0]
arvatav_sona[len(letters)-1] = letters[len(letters)-1]

for i in range (0, len(letters)-1):
    if letters[i] == " ":
        arvatav_sona[i] = " "

print ("Your word is:", *arvatav_sona)
time.sleep(1)

#paneme kõik tsüklisse
for i in range(0,100):
    
#Kontrollib, kas sisestatud on üksik täht
    while True:
        taht = input("Guess the letter: ")
        if taht in "qwertyuiopüõasdfghjklöäzxcvbnm":
            taht = taht.upper()
            pakutud_tahed.append(taht)
            break
            
        else:
            print("This is not a letter!")
              
#Kontrollib, kas täht on sõnas olemas
    if taht in letters:
        print("Sinu pakutud täht on olemas!")
        for i in range(len(letters)):
            if letters[i] == taht:
                arvatav_sona[i] = taht 
    else:
        print("Sinu pakutud tähte ei ole selles sõnas!")
        valesti_pakutud_tahti = valesti_pakutud_tahti + 1

    if "-" not in arvatav_sona:
        print("Sõna on arvatud!")
        print("Your word was:", *arvatav_sona)
        break

    print ("Your word is:", *arvatav_sona)
    print ("Guessed letters:", ", ".join(pakutud_tahed))
    print (" ")



    i = i + 1
    if valesti_pakutud_tahti == 3:
        print("Game over!")
        print("Your word was", word)
        break
            



