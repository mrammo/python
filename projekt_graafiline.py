from tkinter import *
import random
from unittest import TestCase
import os

WORDS = ["COMPUTER", "ENGLISH", "DOCUMENTATION", "SECURITY", "BATTERY", "FOOTBALL", "SCHOOL", "CHILDREN", "TEACHER"]

def getWord(words):   
    word = random.choice(words)
    return(word)


class HangmanGame:

    def __init__(self, master): # master on root ehk main window
        master.title("Hangman Game")
        frame = Frame(master)
        frame.pack()
 
        self.word = getWord(WORDS)
        self.wordList = list(self.word)
        self.guessingWord = ["_"]*len(self.wordList)
        self.guess = None
        self.wrongGuesses = 0
        self.guessedLetters = []

        self.photo0 = PhotoImage(file=os.path.join("pics", "pilt0.gif"))
        self.photo1 = PhotoImage(file=os.path.join("pics", "pilt1.gif"))
        self.photo2 = PhotoImage(file=os.path.join("pics", "pilt2.gif"))
        self.photo3 = PhotoImage(file=os.path.join("pics", "pilt3.gif"))
        self.photo4 = PhotoImage(file=os.path.join("pics", "pilt4.gif"))
        self.photo5 = PhotoImage(file=os.path.join("pics", "pilt5.gif"))
        self.photo6 = PhotoImage(file=os.path.join("pics", "pilt6.gif"))
        self.label = Label(frame, image = self.photo0)
        
        self.guessingWordLabel = Label(frame, text = self.guessingWord, font = "size, 20")
        
        self.message = Label(frame, text = "", font = "size, 10")

        
        self.entry = Entry(frame)
        self.entry.bind("<KeyPress>", self.keyPress)
        self.entry.focus()

        self.guessButton = Button(frame, text="Guess a Letter", command=self.guessLetter)
        
        self.guessed = Label(frame, text="Guessed letters: ")
        
        self.label.grid(row=0, rowspan=5, column=0)
        self.guessingWordLabel.grid(row=0, column=1)
        self.message.grid(row=1, column=1)
        self.entry.grid(row=2, column=1)
        self.guessButton.grid(row=2, column=2)
        self.guessed.grid(row=3, column=1)



    def guessLetter(self):
        #print(self.guess)
        if self.guess == None:
            #print(self.guess)
            self.message.config(text="Try again! Your input is not correct")
        elif (len(self.guess) > 1):
            self.message.config(text="You can entry only one letter!")
        elif self.guess in self.guessedLetters:
            self.message.config(text="You already guessed this letter!")
        elif self.guess in self.word:
            #print("Sinu pakutud täht on olemas!")
            for i in range(len(self.word)):
                if self.word[i] == self.guess:
                    self.guessingWord[i] = self.guess
            self.message.config(text="Very good! %s is in your word!" % (self.guess))
            self.guessedLetters.append(self.guess)
            
        else:
            #print("Sinu pakutud tähte ei ole selles sõnas!")
            self.wrongGuesses += 1
            
            self.message.config(text="Try again! %s is not in your word!" % (self.guess))
            self.guessedLetters.append(self.guess)
             
        self.entry.delete(0,"end")
        self.guess = None
        self.guessingWordLabel.config(text=self.guessingWord)
        self.guessed.config(text="Guessed letters: %s" % (", ".join(str(e) for e in self.guessedLetters)))

        #Changing photo and game over:
        if self.wrongGuesses == 1:
            self.label.config(image = self.photo1)
        elif self.wrongGuesses == 2:
            self.label.config(image = self.photo2)
        elif self.wrongGuesses == 3:
            self.label.config(image = self.photo3)
        elif self.wrongGuesses == 4:
            self.label.config(image = self.photo4)
        elif self.wrongGuesses == 5:
            self.label.config(image = self.photo5)
        elif self.wrongGuesses == 6:
            self.label.config(image = self.photo6)
            self.message.config(text="GAME OVER! Your word was: %s" % (self.word), font = "size, 10")
            self.guessButton.config(state=DISABLED)

        if self.wordList == self.guessingWord:
            self.message.config(text="YOU WON!", font = "size, 15")
            self.guessButton.config(state=DISABLED)

    def keyPress(self, event):
        
        self.message.config(text="")

        if event.char == "":
            return "break"
        elif event.char in "qwertyuiopüõasdfghjklöäzxcvbnm":
            self.guess = (self.entry.get()+event.char)
            self.guess = self.guess.upper()
        elif event.char.isdigit():        
            self.message.config(text="You can entry only letters!")
            return "break"
        
        

if __name__ == "__main__":
    root = Tk()
    root.geometry("700x230")
    b = HangmanGame(root)
    root.mainloop()


class TestCase(TestCase):

    def setUp(self):
        self.test_function = getWord
       
    def testWord(self):
        result=self.test_function(WORDS)
        expected_result_list = ["COMPUTER", "ENGLISH", "DOCUMENTATION", "SECURITY", "BATTERY", "FOOTBALL", "SCHOOL", "CHILDREN", "TEACHER"]
        if result in expected_result_list:
            expected_result = result
        else:
            expected_result = ""
        self.assertEqual(result, expected_result)
        
        





"""
http://stackoverflow.com/questions/3478890/how-to-insert-only-some-specified-characters-in-a-tkinter-entry-widget

How to change Tkinter label text on button press:
http://stackoverflow.com/questions/29828477/how-to-change-tkinter-label-text-on-button-press
"""
