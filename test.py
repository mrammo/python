import unittest
from projekt_graafiline import HangmanGame
from tkinter import *

###Checking if wrongGuesses result is correct:
class TestCase(unittest.TestCase):
    
    def testWrongGuesses(self):
        game = HangmanGame()
        word = "COMPUTER"
        guess = "S"
        expected_result = 1
        result = game.wGuesses("COMPUTER", "S")
        self.assertEqual(result, expected_result)



if __name__ == "__main__":
    unittest.main()
